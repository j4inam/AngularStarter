angular.module('app.controllers', [])

.controller('AppCtrl', ['$scope', '$stateParams',
    function ($scope, $stateParams) {
    	$(document).ready(function() {
            $('body').addClass('loaded');
            $('.button-collapse').sideNav({closeOnClick: true});
        });
    }])

.controller('homeCtrl', ['$scope', '$stateParams',
    function ($scope, $stateParams) {
    	$(document).ready(function() {
            $('body').addClass('loaded');
        });
    }])

.controller('loginCtrl', ['$scope', '$stateParams',
    function ($scope, $stateParams) {
    	$(document).ready(function() {
            $('body').addClass('loaded');
        });
    }])
